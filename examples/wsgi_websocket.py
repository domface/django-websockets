# entry point for the websocket loop
import os
import gevent.monkey
import redis.connection
import gevent.socket

redis.connection.socket = gevent.socket
from ws4redis.uwsgi_runserver import uWSGIWebsocketServer
application = uWSGIWebsocketServer()

#uwsgi --socket /opt/django/django.sock --buffer-size=32768 --workers=5 --master --module wsgi_django
#uwsgi --http-socket /opt/django/web.sock --gevent 1000 --http-websockets --workers=2 --master --module wsgi_websocket